FASTER-RCNN

  - Installation Protobuf
  - Extraire models
  - Edit Config WORK_PATH
  - Dans models/research :
      ```
      protoc object_detection/protos/*.proto --python_out=.
      cp object_detection/packages/tf2/setup.py .
      python -m pip install .
      ```
      
      Lancer Tensorboard :
      ```
      tensorboard --logdir=models/faster_rcnn_inception_resnet_v2
      ```
      
      Lancer le train :
      ```
      python model_main_tf2.py --model_dir=models/faster_rcnn_inception_resnet_v2 --pipeline_config_path=models/faster_rcnn_inception_resnet_v2/pipeline.config
      ```
      
      Exporter le model entrainé
      ```
      python exporter_main_v2.py --input_type image_tensor --pipeline_config_path models/faster_rcnn_inception_resnet_v2/pipeline.config --trained_checkpoint_dir models/faster_rcnn_inception_resnet_v2/ --output_directory final_models/
      ```
