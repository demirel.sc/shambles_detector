import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import warnings
from config import *
import tensorflow as tf
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as viz_utils
import glob

LABEL_PATH = WORK_PATH + "/train_dataset/label_map.pbtxt"
MODEL_PATH = WORK_PATH + "/final_models/saved_model"
IMAGES_PATH = glob.glob(WORK_PATH + "/images/*.jpg")

#load trained model
detect_fn = tf.saved_model.load(MODEL_PATH)
category_index = label_map_util.create_category_index_from_labelmap(LABEL_PATH, use_display_name=True)

for img in IMAGES_PATH:
    image_np = np.array(Image.open(img))
    input_tensor = tf.convert_to_tensor(image_np)
    input_tensor = input_tensor[tf.newaxis, ...]
    detections = detect_fn(input_tensor)
    num_detections = int(detections.pop('num_detections'))
    detections = {key: value[0, :num_detections].numpy()
                   for key, value in detections.items()}
    detections['num_detections'] = num_detections
    detections['detection_classes'] = detections['detection_classes'].astype(np.int64)
    image_np_with_detections = image_np.copy()
    plt.figure()
    viz_utils.visualize_boxes_and_labels_on_image_array(
          image_np_with_detections,
          detections['detection_boxes'],
          detections['detection_classes'],
          detections['detection_scores'],
          category_index,
          use_normalized_coordinates=True,
          max_boxes_to_draw=200,
          min_score_thresh=.30,
          agnostic_mode=False
    )
    plt.imshow(image_np_with_detections)
    plt.savefig('myfilename.png', dpi=100)
    plt.show()


