import tensorflow.compat.v1 as tf
import numpy as np
import pandas as pd
import glob
import xml.etree.ElementTree as ET
import random
import os 
from config import *

# Fill csv core data from xml
def create_csv_data(split):
    files = []
    classes = []
    for xml in split:
        tree = ET.parse(xml)
        root = tree.getroot()
        for child in root.findall('object'):
            classes.append(child[0].text)
            core = (
                root.find('filename').text,
                int(root.find('size')[0].text),
                int(root.find('size')[1].text),
                child[0].text,
                int(child[5][0].text),
                int(child[5][2].text),
                int(child[5][1].text),
                int(child[5][3].text)
            )
            files.append(core)
    columns = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    df = pd.DataFrame(files, columns=columns)
    classes = list(set(classes))
    classes.sort()
    return df, classes

# Get all Annotations files and shuffle order
all_files = glob.glob(WORK_PATH + "/annotations/*.xml")
random.shuffle(all_files)

# Split files into 80% train and 20% test
train_files_label = all_files[:int(len(all_files)*0.8)]
test_files_label = all_files[-int(len(all_files)*0.2):]

# Scrap XML info and create Train/Test csv
for split, label in zip([train_files_label, test_files_label], ["train_labels", "test_labels"]):
    df, classes = create_csv_data(split)
    df.to_csv(WORK_PATH +'/train_dataset/'+f'{label}.csv', index=None)

label_map_path = os.path.join(WORK_PATH+"/train_dataset/", "label_map.pbtxt")
pbtxt_content = ""

for i, class_name in enumerate(classes):
    pbtxt_content = (
        pbtxt_content
        + "item {{\n    id: {0}\n    name: '{1}'\n}}\n\n".format(i + 1, class_name)
    )
pbtxt_content = pbtxt_content.strip()
with open(label_map_path, "w+") as f:
    f.write(pbtxt_content)