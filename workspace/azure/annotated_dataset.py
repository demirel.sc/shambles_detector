# azureml-core version 1.0.72 ou supérieure obligatoire
from azureml.core import Workspace, Dataset, datastore
from azureml.data.datapath import DataPath

workspace = Workspace.from_config()
datastore = workspace.get_default_datastore()

#Recupération des données dans le dossier annotated_images
dataset = Dataset.get_by_name(workspace, name='annotated_images')
dataset.download(target_path='annotated_images/', overwrite=True)

#Envoi du dossier annotated_images dans azure
data_path = DataPath(datastore, 'UI/07-01-2021_025815_UTC/')
updated_dataset = Dataset.File.upload_directory('./annotated_images', data_path)
updated_dataset.register(workspace = workspace,
                        name = 'annotated_images',
                        description = 'Dataset contenant toutes les images labellisées et annotées du projet Shambles',
                        create_new_version = True)
