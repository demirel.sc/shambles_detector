# azureml-core version 1.0.72 ou supérieure obligatoire
from azureml.core import Workspace, Dataset, datastore
from azureml.data.datapath import DataPath

workspace = Workspace.from_config()
datastore = workspace.get_default_datastore()

#Recupération des données dans le dossier raw_images
dataset = Dataset.get_by_name(workspace, name='raw_images')
dataset = Dataset.get_by_name(workspace, name='raw_images').take(4)
#dataset.download(target_path='raw_images/', overwrite=True)
print(dataset)

# #Envoi du dossier raw_images dans azure
# data_path = DataPath(datastore, 'UI/07-01-2021_024423_UTC/')
# updated_dataset = Dataset.File.upload_directory('./raw_images', data_path)
# updated_dataset.register(workspace = workspace,
#                         name = 'raw_images',
#                         description = 'Dataset contenant toutes les images brutes du projet Shambles',
#                         create_new_version = True)
