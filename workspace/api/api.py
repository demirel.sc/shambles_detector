from flask import Flask
from flask import send_file, send_from_directory, safe_join, abort
app = Flask(__name__)
  
@app.route("/")
def home():
    return "<h1>BLBLBLBL</h1>"

@app.route("/getModel")
def sendModel():
    try:
        return send_from_directory("/home/zmajev/TensorFlow/workspace/training/final_models/saved_model", 'saved_model.pb', as_attachment=True)
    except FileNotFoundError:
        abort(404)
    
app.run()